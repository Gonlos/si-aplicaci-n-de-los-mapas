#!/usr/bin/python
import os
import sys

if len(sys.argv) == 1:
  s = ""
else:
  s = "-" + sys.argv[1]

if len(sys.argv) <= 2:
  escala = 1.0
else:
  escala = sys.argv[2]

os.system( """ gnuplot -e "set xlabel 'Numero de elementos'; set ylabel 'Tiempos (ms)'; set grid; plot 
'./valores{0}' using 2:3 title 'Tiempo minimo' with lines,
'./valores{0}' using 2:4 title 'Tiempo maximo' with lines,
'./valores{0}' using 2:5 title 'Tiempo promedio' with lines;
set terminal png large size 1024*{1},576*{1}; set out; replot" > grafica-tiempos{0}.png """.format( s, escala ) )


os.system( """ gnuplot -e "set xlabel 'Numero de elementos'; set ylabel 'Tiempos (ms)'; set grid; plot 
'./valores{0}' using 2:3 title 'Tiempo minimo' with lines;
set terminal png large size 1024*{1},576*{1}; set out; replot" > grafica-tiempos-min{0}.png """.format( s, escala ) )

os.system( """ gnuplot -e "set xlabel 'Numero de elementos'; set ylabel 'Tiempos (ms)'; set grid; plot 
'./valores{0}' using 2:4 title 'Tiempo maximo' with lines;
set terminal png large size 1024*{1},576*{1}; set out; replot" > grafica-tiempos-max{0}.png """.format( s, escala ) )

os.system( """ gnuplot -e "set xlabel 'Numero de elementos'; set ylabel 'Tiempos (ms)'; set grid; plot 
'./valores{0}' using 2:5 title 'Tiempo promedio' with lines;
set terminal png large size 1024*{1},576*{1}; set out; replot" > grafica-tiempos-med{0}.png """.format( s, escala ) )


os.system( """ gnuplot -e "set xlabel 'Numero de elementos'; set ylabel 'Memoria (KB)'; set grid; plot 
'./valores{0}' using 2:6 title 'VM' with lines,
'./valores{0}' using 2:7 title 'RSS' with lines;
set terminal png large size 1024*{1},576*{1}; set out; replot" > grafica-memoria{0}.png """.format( s, escala ) )
