DIROBJ := obj/
DIREXE := exec/
DIRSRC := src/
DIRHEA := $(DIRSRC)
DIRDATA := data/
DIRTMP := tmp/

OBJS := ${DIROBJ}main.o ${DIROBJ}splicemap.o ${DIROBJ}quadtree.o $(DIROBJ)virtualmap.o ${DIROBJ}state.o ${DIROBJ}cache.o ${DIROBJ}frontier.o ${DIROBJ}frontiernodefactory.o

# HDF5 stuff
HDF5INCPATH = -I/usr/local/hdf5/include
HDF5LIBPATH = -L/usr/local/hdf5/lib
HDF5LIB     = -lhdf5 -lhdf5_cpp

#debug option
CFLAGS := -I$(DIRHEA) -c -pedantic -Wall -std=c++11 -g
#normal option
#CFLAGS := -I$(DIRHEA) -c -pedantic -Wall -std=c++11 -O3
LDLIBS := -lzip
CC := g++

all : dirs main

dirs:
	mkdir -p $(DIROBJ) $(DIREXE) $(DIRTMP)

main: $(OBJS)
	$(CC) -o $(DIREXE)$@ $^ $(LDLIBS) $(HDF5LIBPATH) $(HDF5LIB)

$(DIROBJ)%.o: $(DIRSRC)%.cpp
	$(CC) $(CFLAGS) $(HDF5INCPATH) $^ -o $@

createHDF5:
	./exec/main create ./maps/MDT25-0711.zip ./maps/MDT25-0712.zip ./maps/MDT25-0736.zip ./maps/MDT25-0737.zip

install-req: installHDF5
	sudo apt-get install libzip-dev

installHDF5: dirs
	cat other/hdf5-1.8.13.tar.gz | tar xz -C $(DIRTMP)
	cd $(DIRTMP)/hdf5-1.8.13 && ./configure -C  --prefix=/usr/local/hdf5 --enable-cxx
	cd $(DIRTMP)/hdf5-1.8.13 && make 
	cd $(DIRTMP)/hdf5-1.8.13 && make check
	cd $(DIRTMP)/hdf5-1.8.13 && sudo make install
	cd $(DIRTMP)/hdf5-1.8.13 && sudo make check-install
	sudo ln -s /usr/local/hdf5/lib/libhdf5.so.8 /usr/lib
	sudo ln -s /usr/local/hdf5/lib/libhdf5_cpp.so.8 /usr/lib
	rm -R cd $(DIRTMP)/hdf5-1.8.13

clean : 
	rm -rf *~ core $(DIROBJ) $(DIREXE) $(DIRSRC)*~ $(DIRDATA)*~ $(DIRTMP)
