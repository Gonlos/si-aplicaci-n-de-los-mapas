/*
 * =====================================================================================
 *
 *       Filename:  state.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  06/10/14 18:39:33
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Juan Manuel (), JuanManuel.Fernandez5@alu.uclm.es
 *   Organization:  
 *
 * =====================================================================================
 */


#ifndef __STATE__
#define __STATE__

#include <stdlib.h>
#include "structures.h"
#include <string>

enum TState
{
  SUR_OESTE = 0,
  SUR,
  SUR_ESTE,
  OESTE,
  NONE,
  ESTE,
  NOR_OESTE,
  NORTE,
  NOR_ESTE
};

/*
 * =====================================================================================
 *        Class:  State
 *  Description:  
 * =====================================================================================
 */
class State
{
  public:
    /* ====================  LIFECYCLE     ======================================= */
    State ();                             /* constructor */
    ~State ();                             /* destructor */

    /* ====================  ACCESSORS     ======================================= */

    TLugar*  Lugar() const { return _lugar; } 
    void Lugar( TLugar* lugar ) { _lugar = lugar; }
    
    double Altitud() const { return _altitud; } 
    void Altitud( double altitud ) { _altitud = altitud; }
    
    TState Estado() const { return _estado; }
    void Estado( TState estado ) { _estado = estado; } 
    
    TPoint* UTM() const { return _posUTM; }
    void UTM( TPoint* posUTM ) { _posUTM = posUTM; }
     
    int Cellsize() const { return _cellsize; }
    void Cellsize( int cellsize ) { _cellsize = cellsize; }

    bool Equivalent( State* other );
   
    long long GetID() const { return (((long long) _posUTM->y) << 32) | _posUTM->x; }
    
    std::string GetText();

    /* ====================  MUTATORS      ======================================= */

    /* ====================  OPERATORS     ======================================= */

  protected:
    /* ====================  METHODS       ======================================= */

    /* ====================  DATA MEMBERS  ======================================= */

  private:
    /* ====================  METHODS       ======================================= */

    /* ====================  DATA MEMBERS  ======================================= */
    TLugar* _lugar;
    double _altitud;
    TState _estado;

    TPoint* _posUTM;
    int _cellsize;

}; /* -----  end of class State  ----- */

#endif

