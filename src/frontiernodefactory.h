/*
 * =====================================================================================
 *
 *       Filename:  frontiernodefactory.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  27/10/14 23:46:38
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Juan Manuel (), JuanManuel.Fernandez5@alu.uclm.es
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef __FRONTIERNODEFACTORY__
#define __FRONTIERNODEFACTORY__

#include <stdlib.h>
#include "frontier.h"
#include "state.h"

#include <math.h>
#include <string>

/*
 * =====================================================================================
 *        Class:  FrontierNodeFactory
 *  Description:  Create the nodes of the search
 * =====================================================================================
 */
class FrontierNodeFactory
{
  public:
    /* ====================  LIFECYCLE     ======================================= */
    FrontierNodeFactory( State* finalState, std::string& estrategia );   /* constructor */

    /* ====================  ACCESSORS     ======================================= */

    /* ====================  MUTATORS      ======================================= */

    /* ====================  OPERATORS     ======================================= */


    FrontierNode* CreateFirstNode( State* value );
    FrontierNode* CreateNode( State* value, double desnivel, bool changeDirection, FrontierNode* parent );

  protected:
    /* ====================  METHODS       ======================================= */

    /* ====================  DATA MEMBERS  ======================================= */

  private:
    /* ====================  METHODS       ======================================= */

    inline double G( FrontierNode* myNode );
    inline double H( FrontierNode* myNode );
    inline void DepthCost( FrontierNode* myNode );
    inline void WidthCost( FrontierNode* myNode );
    inline void UniformCostCost( FrontierNode* myNode );
    inline void GreedyCost( FrontierNode* myNode );
    inline void AStartCost( FrontierNode* myNode );

    /* ====================  DATA MEMBERS  ======================================= */

    unsigned long long _nextID;

    TPoint* _finalStateUTM;
    void (FrontierNodeFactory::*_costFunction)( FrontierNode* myNode );


}; /* -----  end of class Frontier  ----- */

#endif
