/*
 * =====================================================================================
 *
 *       Filename:  cache.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  24/09/14 02:02:44
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Juan Manuel (), JuanManuel.Fernandez5@alu.uclm.es
 *   Organization:  
 *
 * =====================================================================================
 */

#include "cache.h"

Cache::Cache () {
  _cacheFirstNode = NULL;
  _cacheLastNode = NULL;
  _cacheSize = 0;
  _currentWeight = NUM_ELEMENTOS_MARGEN;
}

Cache::~Cache () {
  TCacheNode* nextNode = _cacheFirstNode;
  TCacheNode* tmpNode;
  while ( nextNode != NULL ) {
    tmpNode = nextNode;
    nextNode = tmpNode->next;
    delete tmpNode;
  }
}

SpliceMap* Cache::Get( int id ) {
  SpliceMap* myT = NULL;
  TCacheNode* nextNode = _cacheFirstNode;
  TCacheNode* searched = NULL;

  while ( nextNode != NULL ) {
    if ( nextNode->id == id ) {
      searched = nextNode;  
      break;
    }
    nextNode = nextNode->next;
  }

  _currentWeight++;
  if ( searched != NULL ) {
    searched->weight = ( _currentWeight - NUM_ELEMENTOS_MARGEN > searched->weight ) ? _currentWeight - NUM_ELEMENTOS_MARGEN + 2 : searched->weight + 2;

    if ( ( searched != _cacheFirstNode ) && ( searched->weight > _currentWeight ) ) {
      // check if the value searched is the last node and change this with the previous
      if ( searched == _cacheLastNode ) _cacheLastNode = searched->back;

      // skip the searched value
      if ( searched->back != NULL ) searched->back->next = searched->next;
      if ( searched->next != NULL ) searched->next->back = searched->back;
      // define the next node of the searched how the first node
      searched->next = _cacheFirstNode;
      searched->back = NULL;
      // define the back value
      _cacheFirstNode->back = searched;
      // define the first node has the searched
      _cacheFirstNode = searched;
    }

    myT = searched->value;
  }

  return myT;
}

void Cache::Add( SpliceMap* value ) {
  TCacheNode* searched = NULL;

  searched = new TCacheNode();
  searched->id = value->GetID();
  searched->next = _cacheFirstNode;
  searched->back = NULL;
  searched->weight = _currentWeight;

  if ( _cacheSize == 0 ) {
    _cacheLastNode = searched;
    _cacheSize++;
  } else {
    _cacheFirstNode->back = searched;
    if ( _cacheSize == MAX_SIZE_CACHE_MAP ) {
      //delete _cacheLastNode->value;
      _cacheLastNode = _cacheLastNode->back;
      delete _cacheLastNode->next;
      _cacheLastNode->next = NULL;
    } else {
      _cacheSize++;
    }
  }
  
  _cacheFirstNode = searched;

  searched->value = value;
}
