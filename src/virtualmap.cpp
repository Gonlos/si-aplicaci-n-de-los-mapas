/*
 * =====================================================================================
 *
 *       Filename:  virtualmap.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  24/09/14 02:02:44
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Juan Manuel (), JuanManuel.Fernandez5@alu.uclm.es
 *   Organization:  
 *
 * =====================================================================================
 */

#include "virtualmap.h"

#define DEL_HDF5(X) if (X != NULL) { X->close(); delete X; X = NULL; }
#define GET_QUADRANT2(P_X, P_Y, MID_X, MID_Y) ((( (P_X) < (MID_X)) ? 0 : 1) + (((P_Y) < (MID_Y)) ? 2 : 0))

// codigo temporal para las estadisticas
void process_mem_usage(double& vm_usage, double& resident_set)
{
   using std::ios_base;
   using std::ifstream;
   using std::string;

   vm_usage     = 0.0;
   resident_set = 0.0;

   // 'file' stat seems to give the most reliable results
   //
   ifstream stat_stream("/proc/self/stat",ios_base::in);

   // dummy vars for leading entries in stat that we don't care about
   //
   string pid, comm, state, ppid, pgrp, session, tty_nr;
   string tpgid, flags, minflt, cminflt, majflt, cmajflt;
   string utime, stime, cutime, cstime, priority, nice;
   string O, itrealvalue, starttime;

   // the two fields we want
   //
   unsigned long vsize;
   long rss;

   stat_stream >> pid >> comm >> state >> ppid >> pgrp >> session >> tty_nr
               >> tpgid >> flags >> minflt >> cminflt >> majflt >> cmajflt
               >> utime >> stime >> cutime >> cstime >> priority >> nice
               >> O >> itrealvalue >> starttime >> vsize >> rss; // don't care about the rest

   stat_stream.close();

   long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
   vm_usage     = vsize / 1024.0;
   resident_set = rss * page_size_kb;
}

// codigo temporal para las estadisticas
std::string to_human_unit( double v ) {
  int i = 0;
  while ( ( v >= 1024 ) && (i <= 2) ) {
    v /= 1024;
    i++;
  }

  switch ( i ) {
    case 0:
      return std::to_string( v ) + " KB";
      break;
    case 1:
      return std::to_string( v ) + " MB";
      break;
  }
  
  return std::to_string( v ) + " GB";
}

VirtualMap::VirtualMap ( std::string fileName ) {
  _initialState = NULL;
  _finalState = NULL;
  _h5fileName = fileName;
  _h5file = NULL;
}

VirtualMap::~VirtualMap () {
  CloseFile();
}

TSolution* VirtualMap::SearchSolution ( int maxDepth, std::string strategy, bool trimming ) {
  FrontierNodeFactory myNodeFactory( _finalState, strategy );
  Frontier frontera( trimming, strategy );

  bool error;
  frontera.Add( myNodeFactory.CreateFirstNode( _initialState ) );

  FrontierNode* myNode;
  TSuccessorAndState mySuccessors[LENGTH_PLACES_X * LENGTH_PLACES_Y];
  int lSuccessors;

  // --------------------------------------------------------------
  // variables a borrar cuando ya no se necesite las estadisticas
  long long a = 0;
  long long numNodos = 0;
  long millis;
  timeb time1, time2;

  double vm, rss;
  // --------------------------------------------------------------


  bool solucionEncontrada = false;
  FrontierNode* myFrontierNode;

  ftime(&time1);

  try {
    while ( ( myNode = frontera.PopFirst() ) != NULL ) {
      if ( Meta( myNode->estado ) ) {
        solucionEncontrada = true;
        break;
      }

      if ( myNode->depth >= maxDepth ) continue;

      lSuccessors = Successors( myNode->estado, mySuccessors );

      for ( int i = 0; i < lSuccessors; i++ ) {
        myFrontierNode = myNodeFactory.CreateNode( mySuccessors[i].state, mySuccessors[i].diffAltitude, mySuccessors[i].diffDirection, myNode );
        if ( frontera.Add( myFrontierNode ) == 0 ) {
          numNodos++;
        } else {
          delete myFrontierNode->estado;
          delete myFrontierNode;
        }
      }

      a++;
    }
  } catch (int n) {

  }


  ftime(&time2);
  millis = ( time2.millitm + (time2.time & 0xfffff) * 1000 ) - ( time1.millitm + (time1.time & 0xfffff) * 1000 );

  // si hemos encontrado la solución creamos la misma
  TSolution* solution = new TSolution;
  if ( solucionEncontrada ) {
    solution->depth = MakeSolution( myNode, &solution->path );
    solution->cost = myNode->cost;
  } else {
    solution->depth = 0;
  }

/*
 *  process_mem_usage(vm, rss);
 *
 *  std::cout << "¿Se ha encontrado la solución?: " << (solucionEncontrada ? "Si" : "No") << std::endl;
 *
 *  if ( solucionEncontrada ) {
 *    std::cout << "Numero de elementos de la solución: " << std::to_string( solution->depth ) << std::endl;
 *    std::cout << "Coste de la solución: " << std::to_string( myNode->cost ) << std::endl;
 *  }
 *
 *  std::cout << "Numero de nodos explorados: " << std::to_string( a ) << std::endl;
 *  std::cout << "Numero de nodos insertados: " << std::to_string( numNodos ) << std::endl;
 *  std::cout << "Tiempo total: " << std::to_string( millis ) << "ms" << std::endl;
 *  std::cout << "VM: " << to_human_unit(vm) << "; RSS: " << to_human_unit(rss) << std::endl;
 *
 */
  return solution;
}

int VirtualMap::MakeSolution( FrontierNode* myNode, State*** result ) {
  int l = myNode->depth+1;
  *result = new State*[ l ];

  State** currentNodeResult = (*result) +l-1;
  for( FrontierNode* currentNode = myNode; currentNode != NULL; currentNode = currentNode->parent ) {
    *currentNodeResult = currentNode->estado;
    currentNode->estado = NULL;
    currentNodeResult--;
  }

  return l;
}

void VirtualMap::ReadFile (  ) {
  _h5file = new H5::H5File( _h5fileName, H5F_ACC_RDONLY );
}

void VirtualMap::CloseFile ( ) {
  if ( _h5file != NULL ) {
    _h5file->close();
  }
}

bool VirtualMap::Meta( State* myState ) {
  return _finalState->Equivalent( myState );
}

void VirtualMap::Init(char* zips[]) {
  std::vector<IQuadTree*>* v = new std::vector<IQuadTree*>;
  SpliceMap* p;

  for( char** it = zips; *it; it++ ) {
    p = new SpliceMap();
    p->FileDir( *it );
    p->ReadHeader();
    p->ObtainPageIdZip();
    v->push_back( p );
  }
 
  _quadtree.Add(*v);
  delete v;

  _quadtree.Balance();

  H5::H5File file( _h5fileName, H5F_ACC_TRUNC );
  _quadtree.H5File( &file );
  _quadtree.Save();
  _quadtree.Clear();
  file.close();
}

void VirtualMap::LoadData() {
  _quadtree.H5File( _h5file );
  _quadtree.Load();
}

TLugar* VirtualMap::LugarGeografico(int utmX, int utmY) {
  return _quadtree.LugarGeografico( utmX, utmY );
}

SpliceMap* VirtualMap::GetSpliceMap( int id ) {
  bool error = true;
  SpliceMap* mySpliceMap = _cache.Get( id );

  if ( mySpliceMap == NULL ) {
    mySpliceMap = new SpliceMap();
   
    try
    {
      H5::Exception::dontPrint();

      H5::Group myGroup( _h5file->openGroup( H5_PATH_MAPS "/" + std::to_string( id ) ) );

      error = !mySpliceMap->ReadGroup( myGroup );

      if ( !error ) {
        _quadtree.SetVirtualMap( mySpliceMap, myGroup.getLinkval( H5_LINK_PAGE ) );
        _cache.Add( mySpliceMap );
      }
    }
    catch( H5::Exception& e) 
    {
    }

    if ( error ) return NULL;
  }

  return mySpliceMap;
}

double VirtualMap::Altitud(TLugar &lugar, bool& error) {
  SpliceMap* mySM = GetSpliceMap( lugar.hoja );
  error = mySM == NULL;

  return ( mySM == NULL ) ? 0.0 : mySM->GetData( lugar.x, lugar.y );
}

void VirtualMap::UTM(TLugar &lugar, int& utmX, int& utmY, bool &error) {
  SpliceMap* mySM = GetSpliceMap( lugar.hoja );
  error = mySM == NULL;

  if ( error ) return;

  utmX = mySM->GetCornerLowLeft()->x + lugar.x * mySM->GetCellSize();
  utmY = mySM->GetCornerLowLeft()->y + lugar.y * mySM->GetCellSize(); 

  error = (lugar.x < 0) || (lugar.y < 0) || ( lugar.x *mySM->GetCellSize() >= mySM->GetWidth() ) || ( lugar.y *mySM->GetCellSize() >= mySM->GetHeight() );
}

int VirtualMap::Successors( State* currentState, TSuccessorAndState* result ) {
  SpliceMap* mySpliceMap = GetSpliceMap( currentState->Lugar()->hoja );

  TResultNeighbor myNeighbors[LENGTH_PLACES_X * LENGTH_PLACES_Y];

  int l = _quadtree.Neighbors( currentState->UTM()->x, currentState->UTM()->y, mySpliceMap, &myNeighbors[0] );

  for (int i = 0; i<l; i++) {
    // !fixme: comprobar si es nulo luego
    SpliceMap* mySM = GetSpliceMap( myNeighbors[i].lugar->hoja );

    result->state = new State();
    result->state->Lugar( myNeighbors[i].lugar );
    result->state->Altitud( mySM->GetData( myNeighbors[i].lugar->x, myNeighbors[i].lugar->y ) );
    result->state->Estado( myNeighbors[i].action );
    
    result->state->UTM( new TPoint{ mySM->GetCornerLowLeft()->x + myNeighbors[i].lugar->x * mySM->GetCellSize(), 
                                    mySM->GetCornerLowLeft()->y + myNeighbors[i].lugar->y * mySM->GetCellSize() } );


    result->state->Cellsize( mySM->GetCellSize() );
    
    result->action = myNeighbors[i].action;

    result->diffDirection = currentState->Estado() == result->state->Estado();

    result->diffAltitude = fabs( currentState->Altitud() - result->state->Altitud() );
    
    result++;
  }

  return l;
}
