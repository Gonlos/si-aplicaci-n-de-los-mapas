/*
 * =====================================================================================
 *
 *       Filename:  virtualmap.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  24/09/14 02:03:04
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Juan Manuel (), JuanManuel.Fernandez5@alu.uclm.es
 *   Organization:  
 *
 * =====================================================================================
 */


#ifndef __VIRTUALMAP__
#define __VIRTUALMAP__

#include <strtk.hpp>
#include <stdlib.h>
#include "H5Cpp.h"
#include "quadtree.h"
#include "cache.h"
#include "splicemap.h"
#include "frontiernodefactory.h"
#include "frontier.h"

#include <boost/algorithm/string/predicate.hpp>
#include <boost/lexical_cast.hpp>

#include "state.h"
#include "structures.h"

#include <sys/timeb.h>

#include <unistd.h>
#include <ios>
#include <iostream>
#include <fstream>
#include <string>

struct TSolution {
  State** path;
  int depth;
  double cost;
};				/* ----------  end of struct TSolution  ---------- */

typedef struct TSolution TSolution;


struct TSuccessors {
  TLugar* places[LENGTH_PLACES_Y][LENGTH_PLACES_X];
  TPoint cornerLowLeftUTM;
  int cellsize;
  int remaining;
};				/* ----------  end of struct TSuccessors  ---------- */

typedef struct TSuccessors TSuccessors;


struct TSuccessorAndState {
  State* state;
  bool diffDirection;
  double diffAltitude;
  TState action;
};				/* ----------  end of struct TSuccessorAndState  ---------- */

typedef struct TSuccessorAndState TSuccessorAndState;

/*
 * =====================================================================================
 *        Class:  VirtualMap
 *  Description:  
 *
 * =====================================================================================
 */
class VirtualMap
{
  public:
    /* ====================  LIFECYCLE     ======================================= */
    VirtualMap ( std::string file );            /* constructor */
    ~VirtualMap ();                             /* destructor */
    
    void Init(char* zips[]);
    TLugar* LugarGeografico(int utmX, int utmY);
    double Altitud(TLugar &lugar, bool& error);
    bool Meta( State* myState );
    //std::list<TSuccessorAndState*>* Successors( State* currentState );
    int Successors( State* currentState, TSuccessorAndState* result );
    void UTM(TLugar &lugar, int& utmX, int& utmY, bool &error);

    void ReadFile (  );
    void CloseFile ( );

    void LoadData();

    State* InitialState() const { return _initialState; }
    State* FinalState() const { return _finalState; }

    void InitialState( State* myState ) { _initialState = myState; }
    void FinalState( State* myState ) { _finalState = myState; }


    TSolution* SearchSolution( int maxDepth, std::string strategy, bool trimming );

    /* ====================  ACCESSORS     ======================================= */

    /* ====================  MUTATORS      ======================================= */

    /* ====================  OPERATORS     ======================================= */

  protected:
    /* ====================  METHODS       ======================================= */

    /* ====================  DATA MEMBERS  ======================================= */

  private:
    /* ====================  METHODS       ======================================= */

    int MakeSolution( FrontierNode* myNode, State*** result );

    SpliceMap* GetSpliceMap( int id );

    std::string GetGroupName(const H5::Group& ds);
    H5::Group* GetParentGroup(const H5::Group& childGroup, std::string &currentName);

    TLugar* LugarGeografico(const H5::Group* nodeGroup, int utmX, int utmY);

    /* ====================  DATA MEMBERS  ======================================= */

    std::string _h5fileName;
    H5::H5File* _h5file;

    State* _initialState;
    State* _finalState;

    QuadTree _quadtree;
    Cache _cache;

}; /* -----  end of class VirtualMap  ----- */

#endif
