/*
 * =====================================================================================
 *
 *       Filename:  state.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  06/10/14 18:39:08
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Juan Manuel (), JuanManuel.Fernandez5@alu.uclm.es
 *   Organization:  
 *
 * =====================================================================================
 */

#include "state.h"

State::State() {
  _lugar = NULL;
  _altitud = 0.0;
  _estado = TState( 4 );
  _posUTM = NULL;
  _cellsize = 0;
}

State::~State() {
  if ( _lugar != NULL ) delete _lugar;
  if ( _posUTM != NULL ) delete _posUTM;
}

// check if two states are equivalent in position
bool State::Equivalent( State* other ) {
  TPoint* otherUTM = other->UTM();
  int otherCellsize = other->Cellsize();
  return (( _posUTM->x <= otherUTM->x ) && ( _posUTM->y <= otherUTM->y ) && 
          ( otherUTM->x < _posUTM->x + _cellsize ) && ( otherUTM->y < _posUTM->y + _cellsize )) ||
         (( otherUTM->x <= _posUTM->x ) && ( otherUTM->y <= _posUTM->y ) && 
          ( _posUTM->x < otherUTM->x + otherCellsize ) && ( _posUTM->y < otherUTM->y + otherCellsize ));
}

std::string State::GetText() {
  std::string s = "";
  if ( _posUTM != NULL )
    s = "X: ( " + std::to_string( _posUTM->x ) + " ), Y: ( " + std::to_string( _posUTM->y ) + " ), Altitud: ( " + std::to_string( _altitud ) + " )";
  return s;
}

/*
 *std::string State::GetText() {
 *  std::string s = "";
 *  if (_lugar != NULL) {
 *    s += "Mapa: " + std::to_string( _lugar->hoja ) + "\n";
 *    s += "Indice X: " + std::to_string( _lugar->x ) + "\n";
 *    s += "Indice Y: " + std::to_string( _lugar->y ) + "\n";
 *  }
 *
 *  s += "Altitud: " + std::to_string( _altitud ) + "\n";
 *  s += "Estado: ";
 *  s += (_estado == 0) ? "Suroeste" : (
 *       (_estado == 1) ? "Sur" : (
 *       (_estado == 2) ? "Sureste" : (
 *       (_estado == 3) ? "Oeste" : (
 *       (_estado == 4) ? "Mismo" : (
 *       (_estado == 5) ? "Este" : (
 *       (_estado == 6) ? "Noroeste" : (
 *       (_estado == 7) ? "Norte" : (
 *       (_estado == 8) ? "Noreste" : "Unknow" ))))))));
 *  s += "\n";
 *
 *  return s;
 *}
 */
