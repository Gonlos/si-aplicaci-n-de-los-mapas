/*
 * =====================================================================================
 *
 *       Filename:  frontiernodefactory.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  28/10/14 03:56:02
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Juan Manuel (), JuanManuel.Fernandez5@alu.uclm.es
 *   Organization:  
 *
 * =====================================================================================
 */

#include "frontiernodefactory.h"

inline double FrontierNodeFactory::G( FrontierNode* myNode ) {
  int dist = myNode->parent->estado->Cellsize();
  dist *= dist;
  switch ( myNode->action ) {
    case SUR_OESTE:
    case SUR_ESTE:
    case NOR_OESTE:
    case NOR_ESTE:
      dist *= 2;
      break;
    default:
      break;
  }
 
  double cost = sqrtf( dist + myNode->desnivel * myNode->desnivel );

  return myNode->parent->cost + ( ( myNode->changeDirection ) ? cost*1.1 : cost );
}

inline double FrontierNodeFactory::H( FrontierNode* myNode ) {
  TPoint* posUTM = myNode->estado->UTM();
  int a = posUTM->x - _finalStateUTM->x;
  int b = posUTM->y - _finalStateUTM->y;

  return sqrtf(a*a + b*b);
}

inline void FrontierNodeFactory::DepthCost( FrontierNode* myNode ) {
  myNode->assessment = - (double) myNode->depth;
}

inline void FrontierNodeFactory::WidthCost( FrontierNode* myNode ) {
  myNode->assessment = myNode->depth;
}

inline void FrontierNodeFactory::UniformCostCost( FrontierNode* myNode ) {
  myNode->assessment = myNode->cost;
}

inline void FrontierNodeFactory::GreedyCost( FrontierNode* myNode ) {
  myNode->assessment = H( myNode );
}

inline void FrontierNodeFactory::AStartCost( FrontierNode* myNode ) {
  myNode->assessment = myNode->cost + H( myNode );
}

FrontierNodeFactory::FrontierNodeFactory( State* finalState, std::string& estrategia ) {
  _nextID = 0;
  _finalStateUTM = finalState->UTM();
  
  if ( estrategia == "profundidad" ) {
    _costFunction = &FrontierNodeFactory::DepthCost;
  } else if ( estrategia == "anchura" ) {
    _costFunction = &FrontierNodeFactory::WidthCost;
  } else if( estrategia == "uniforme" ) {
    _costFunction = &FrontierNodeFactory::UniformCostCost;
  } else if ( estrategia == "voraz" ) {
    _costFunction = &FrontierNodeFactory::GreedyCost;
  } else {
  //} else if ( estrategia == "A" ) {
    _costFunction = &FrontierNodeFactory::AStartCost;
  }
}

FrontierNode* FrontierNodeFactory::CreateFirstNode( State* value ) {
  FrontierNode* myNode = new FrontierNode;
  myNode->id = _nextID++;
  myNode->parent = NULL;
  myNode->depth = 0;
  myNode->estado = value;
  myNode->action = value->Estado();
  myNode->assessment = 0.0;
  myNode->cost = 0.0;
  myNode->desnivel = 0.0;
  myNode->changeDirection = true;

  return myNode;
}

FrontierNode* FrontierNodeFactory::CreateNode( State* value, double desnivel, bool changeDirection, FrontierNode* parent ) {
  FrontierNode* myNode = new FrontierNode;
  myNode->id = _nextID++;
  myNode->parent = parent;
  myNode->depth = parent->depth+1;
  myNode->estado = value;
  myNode->action = value->Estado();
  myNode->desnivel = desnivel;
  myNode->changeDirection = changeDirection;

  myNode->cost = G( myNode );

  // set the assessment by the strantegy
  (this->*_costFunction)( myNode );

  return myNode;
}
