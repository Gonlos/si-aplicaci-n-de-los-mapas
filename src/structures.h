#ifndef __STRUCTURES__
#define __STRUCTURES__

#define H5_PATH_QUADTREE "/QuadTree"
#define H5_PATH_MAPS "/Maps"
#define H5_CORNER_LOW_LEFT "CornerLowLeft"
#define H5_WIDTH_HEIGHT "WidthHeight"
#define H5_PAGE_NAME "PageName"
#define H5_LINK_PAGE "LinkPage"
#define H5_CELLSIZE "CellSize"
#define H5_PAGE_VALUES "Values"
#define H5_FILE_NAME "Datos.h5"
#define MAX_SIZE_CACHE_MAP 8

#define LENGTH_PLACES_X 3 
#define LENGTH_PLACES_Y 3 

struct TPoint;
typedef struct TPoint TPoint;

struct TPoint {
  int x;
  int y;
};

struct TLugar;
typedef struct TLugar TLugar;

struct TLugar {
  int hoja;
  int x;
  int y;
};

#endif
