/*
 * =====================================================================================
 *
 *       Filename:  frontier.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  27/10/14 23:46:38
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Juan Manuel (), JuanManuel.Fernandez5@alu.uclm.es
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef __FRONTIER__
#define __FRONTIER__

#include <stdlib.h>
#include <time.h>
#include <queue>          // std::priority_queue

#include "state.h"
#include "khash.h"
#include "structures.h"

#include <vector>

struct FrontierNode {
  unsigned long long id;
  unsigned long depth;
  FrontierNode* parent;
  State* estado;
  TState action;
  double assessment;
  double desnivel;
  double cost;
  bool changeDirection;
};				/* ----------  end of struct FrontierNode  ---------- */

typedef struct FrontierNode FrontierNode;

struct HashListFrontierNode {
  FrontierNode* value;
  HashListFrontierNode* next;
};				/* ----------  end of struct HashListFrontierNode  ---------- */

typedef struct HashListFrontierNode HashListFrontierNode;

struct HashListMovements {
  HashListFrontierNode* movement[ 9 ];
};

typedef struct HashListMovements HashListMovements;

struct LessFrontierNode {
  bool operator()( const FrontierNode* a, const FrontierNode* b) const {
    return a->assessment > b->assessment;
  }
};

enum TTrimming {
  TRIM_OFF,
  TRIM_ON_LOW_COST,
  TRIM_ON_VISITED
};

KHASH_MAP_INIT_INT64(longlong, HashListMovements*)

/*
 * =====================================================================================
 *        Class:  Frontier
 *  Description:  Save the nodes of the search
 * =====================================================================================
 */
class Frontier
{
  public:
    /* ====================  LIFECYCLE     ======================================= */
    Frontier( bool poda, std::string& estrategia );  /* constructor */
    ~Frontier ();                                   /* destructor */

    /* ====================  ACCESSORS     ======================================= */

    /* ====================  MUTATORS      ======================================= */

    /* ====================  OPERATORS     ======================================= */

    int Add( FrontierNode* myNode );
    FrontierNode* PopFirst();

  protected:
    /* ====================  METHODS       ======================================= */

    /* ====================  DATA MEMBERS  ======================================= */

  private:
    /* ====================  METHODS       ======================================= */

    /* ====================  DATA MEMBERS  ======================================= */

    TTrimming _trimState;

    khash_t(longlong)* _hashTable;
    std::priority_queue< FrontierNode*, std::vector<FrontierNode*>, LessFrontierNode >* _priorityQueue;

}; /* -----  end of class Frontier  ----- */

#endif
