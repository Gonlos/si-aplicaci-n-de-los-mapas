/*
 * =====================================================================================
 *
 *       Filename:  cache.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  26/10/14 23:28:04
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Juan Manuel (), JuanManuel.Fernandez5@alu.uclm.es
 *   Organization:  
 *
 * =====================================================================================
 */


#ifndef __CACHE__
#define __CACHE__

//#include <strtk.hpp>
#include <stdlib.h>
#include "quadtree.h"
//#include "splicemap.h"

//#include "state.h"
#include "structures.h"

#define NUM_ELEMENTOS_MARGEN 80

struct TCacheNode {
  TCacheNode* back;
  TCacheNode* next;
  int id;
  unsigned long long weight;
  SpliceMap* value;
};				/* ----------  end of struct TCacheNode  ---------- */

typedef struct TCacheNode TCacheNode;


/*
 * =====================================================================================
 *        Class:  Cache
 *  Description:  
 *
 * =====================================================================================
 */

class Cache
{
  public:
    /* ====================  LIFECYCLE     ======================================= */
    Cache ();            /* constructor */
    ~Cache ();                             /* destructor */

    SpliceMap* Get( int id );
    void Add( SpliceMap* value);
    
    /* ====================  ACCESSORS     ======================================= */

    /* ====================  MUTATORS      ======================================= */

    /* ====================  OPERATORS     ======================================= */

  protected:
    /* ====================  METHODS       ======================================= */

    /* ====================  DATA MEMBERS  ======================================= */

  private:
    /* ====================  METHODS       ======================================= */


    TCacheNode* _cacheFirstNode;
    TCacheNode* _cacheLastNode;
    int _cacheSize;

    unsigned long long _currentWeight;

}; /* -----  end of class VirtualMap  ----- */

#endif
