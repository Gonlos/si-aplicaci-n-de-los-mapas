/*
 * =====================================================================================
 *
 *       Filename:  quadtree.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  24/09/14 02:24:17
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Juan Manuel (), JuanManuel.Fernandez5@alu.uclm.es
 *   Organization:  
 *
 * =====================================================================================
 */

#include "quadtree.h"

#define POW2(X, Y) ((long long)(X)*(long long)(X) + (long long)(Y)*(long long)(Y))
#define PITAGORAS(X, Y) sqrtf(POW2(X, Y))

#define CLEAR_VECTOR(TYP, V) for ( std::vector<TYP>::iterator it = (V).begin(); it != (V).end(); it++ ) delete *it;
#define GET_QUADRANT(P, MID_X, MID_Y) ((((P).cornerLowLeft.x < (MID_X)) ? 0 : 1) + (((P).cornerLowLeft.y < (MID_Y)) ? 2 : 0))

bool CompareUpperPointDoubleX (double a, TNode* b)
{
  return a < b->cornerLowLeft.x;
}

bool CompareLowerPointDoubleX (TNode* a, double b)
{
  return a->cornerLowLeft.x < b;
}

bool CompareLowerPointIntX (TNode* a, int b)
{
  return a->cornerLowLeft.x < b;
}

bool CompareLowerPointIntY (TNode* a, int b)
{
  return a->cornerLowLeft.y < b;
}

bool CompareLowerPointX (TNode* a, double b)
{
  return a->cornerLowLeft.x < b;
}

bool CompareLowerPointX (TNode* a, TPoint* b)
{
  return a->cornerLowLeft.x < b->x;
}

bool CompareLowerPointY (TNode* a, TPoint* b)
{
  return a->cornerLowLeft.y < b->y;
}

bool CompareSortPointX (TNode* a , TNode* b)
{
  return a->cornerLowLeft.x < b->cornerLowLeft.x;
}

bool CompareSortPointY (TNode* a , TNode* b)
{
  return a->cornerLowLeft.y < b->cornerLowLeft.y;
}

TLugar* QuadTree::LugarGeografico(int utmX, int utmY) {
  TLugar* resultado = NULL;
  TNode* currentNode = _rootNode;
  int indx;
 
  while ( currentNode != NULL ) {
    indx = (( utmX < currentNode->cornerLowLeft.x ) ? 0 : 1) + (( utmY < currentNode->cornerLowLeft.y ) ? 2 : 0);
    // if de quadrant is the 1 ( up-right ) and the search point is inside
    if ( (indx == 1) &&
         (utmX < currentNode->cornerLowLeft.x + currentNode->width ) && 
         (utmY < currentNode->cornerLowLeft.y + currentNode->height) ) {

      resultado = new TLugar;
      resultado->hoja = currentNode->id;
      resultado->x = ( utmX - currentNode->cornerLowLeft.x ) / currentNode->cellSize;
      resultado->y = ( utmY - currentNode->cornerLowLeft.y ) / currentNode->cellSize;

      return resultado;
    }

    // if not we chose the node of the quadrant
    currentNode = currentNode->childrens[ indx ];
  }
  
  return resultado;
}

bool QuadTree::InitStructNeighbors( int utmX, int utmY, int cellSize, TNeighbors* result ) {
  for ( int j = 0; j < LENGTH_PLACES_Y; j++ )  
    for ( int i = 0; i < LENGTH_PLACES_X; i++ )
      result->places[j][i] = NULL;

  TLugar dummy;
  result->places[ (LENGTH_PLACES_Y-1) / 2 ][ (LENGTH_PLACES_X-1) / 2 ] = &dummy;

  result->remaining = LENGTH_PLACES_X * LENGTH_PLACES_Y -1;

  result->cellSize = cellSize;

  result->cornerLowLeftUTM.x = utmX - result->cellSize * ((LENGTH_PLACES_X-1)/2);
  result->cornerLowLeftUTM.y = utmY - result->cellSize * ((LENGTH_PLACES_Y-1)/2);

  return true;
}


int QuadTree::Neighbors( int utmX, int utmY, IQuadTree* map, TResultNeighbor* result ) {
  TNeighbors resultPartial;
  if ( !InitStructNeighbors( utmX, utmY, map->GetCellSize(), &resultPartial ) ) return -1;

  SearchNeighbors( map->GetQTNode(), &resultPartial );

  resultPartial.places[ (LENGTH_PLACES_Y - 1) / 2 ][ (LENGTH_PLACES_X - 1) / 2 ] = NULL;
  int n = 0;
  for ( int j = 0; j<LENGTH_PLACES_Y; j++ )
    for ( int i = 0; i<LENGTH_PLACES_X; i++ )
      if ( resultPartial.places[j][i] != NULL ) {
        result->lugar = resultPartial.places[j][i];
        result->action = TState( j * LENGTH_PLACES_X + i );
        
        result++;
        n++;
      }

  return n;
}

void QuadTree::SearchNeighbors( TNode* firstNode, TNeighbors* result ) {
  std::stack<TNode*> myNodeStack;
  TNode* parentNode = firstNode;
  TNode* currentNode = NULL;

  int indx;
  while ( true ) {
    currentNode = parentNode;
    parentNode = parentNode->parent; 
    
    if (parentNode == NULL) break;

    indx = ( ( result->cornerLowLeftUTM.y + result->cellSize * (LENGTH_PLACES_Y-1) < parentNode->cornerLowLeft.y ) ? 2 : 0 ) +
           ( ( result->cornerLowLeftUTM.x < parentNode->cornerLowLeft.x ) ? 0 : 1 );

    if ( ( ( ( result->cornerLowLeftUTM.y < parentNode->cornerLowLeft.y ) ? 2 : 0 ) +
           ( ( result->cornerLowLeftUTM.x + result->cellSize * (LENGTH_PLACES_X-1) < parentNode->cornerLowLeft.x ) ? 0 : 1 ) == indx ) && 
         ( parentNode->childrens[ indx ] == currentNode ) ) break;
  }

  myNodeStack.push( currentNode );

  int x0;
  int x1;
  int y0;
  int y1;

  TLugar* lugar;
  
  int j0;
  int j1;
  int i0;
  int i1;

  while ( !myNodeStack.empty() ) {
    currentNode = myNodeStack.top();
    myNodeStack.pop();
    
    // view if the page intersect with the requested matrix
    x0 = std::max( result->cornerLowLeftUTM.x, currentNode->cornerLowLeft.x );
    x1 = std::min( result->cornerLowLeftUTM.x + result->cellSize * LENGTH_PLACES_X , currentNode->cornerLowLeft.x + currentNode->width );

    y0 = std::max( result->cornerLowLeftUTM.y, currentNode->cornerLowLeft.y );
    y1 = std::min( result->cornerLowLeftUTM.y + result->cellSize * LENGTH_PLACES_Y , currentNode->cornerLowLeft.y + currentNode->height );

    // obtain the intersection with the result
    for ( int y = y0; y<y1; y+=result->cellSize ) {
      for ( int x = x0; x<x1; x+=result->cellSize ) {
        if ( result->places[ ( y - result->cornerLowLeftUTM.y ) / result->cellSize ][ ( x - result->cornerLowLeftUTM.x ) / result->cellSize ] == NULL ) {
          lugar = new TLugar();
          lugar->x = ( x - currentNode->cornerLowLeft.x ) / currentNode->cellSize;
          lugar->y = ( y - currentNode->cornerLowLeft.y ) / currentNode->cellSize;
          lugar->hoja = currentNode->id;

          result->places[ ( y - result->cornerLowLeftUTM.y ) / result->cellSize ][ ( x - result->cornerLowLeftUTM.x ) / result->cellSize ] = lugar;
          result->remaining--;
        }
      }
    }

    // if we dont have the complete result
    if ( !result->remaining ) return;

    j0 = ( result->cornerLowLeftUTM.y + result->cellSize * ( LENGTH_PLACES_Y-1 ) < currentNode->cornerLowLeft.y ) ? 1 : 0;
    j1 = ( result->cornerLowLeftUTM.y < currentNode->cornerLowLeft.y ) ? 1 : 0;

    i0 = ( result->cornerLowLeftUTM.x < currentNode->cornerLowLeft.x ) ? 0 : 1;
    i1 = ( result->cornerLowLeftUTM.x + result->cellSize * ( LENGTH_PLACES_X-1 ) < currentNode->cornerLowLeft.x ) ? 0 : 1;

    // go to the childrens who may have the searched page
    for ( int j = j0; j <= j1; j++ )
      for ( int i = i0; i <= i1; i++ )
        if ( currentNode->childrens[ j*2 + i ] != NULL )
          myNodeStack.push( currentNode->childrens[ j*2 + i ] );

  }
}

// function that return the path to the group
std::string QuadTree::GetGroupName(const H5::Group& ds)
{
  size_t len = H5Iget_name(ds.getId(),NULL,0);
  char *buffer = new char(len+1);
  H5Iget_name(ds.getId(),buffer,len+1);
  std::string s = buffer;
  delete buffer;
  return s;
}


void QuadTree::SetVirtualMap(IQuadTree* sm, const std::string& groupDir) {
  TNode* currentNode = _rootNode;
  int l = groupDir.length();
  for ( int i = strlen(H5_PATH_QUADTREE) + 1; i <= l; i+=2 ) {
    currentNode = currentNode->childrens[ groupDir[i] - '0' ];
  }
  
  if ( currentNode->value != NULL ) delete currentNode->value;
  currentNode->value = sm;
  sm->SetQTNOde( currentNode );
}

QuadTree::QuadTree() {
  _rootNode = NULL;
  _vectorSortX = new std::vector<TNode*>;
  _vectorSortY = new std::vector<TNode*>;
}

QuadTree::~QuadTree() {
  Clear();
  delete _vectorSortX;
  delete _vectorSortY;
}

// add new elements to the quadtree
bool QuadTree::Add(std::vector<IQuadTree*> &vOrig ) { 
  std::vector<TNode*> vSort( vOrig.size() );
  std::vector<TNode*>* vTmp;

  std::vector<IQuadTree*>::iterator itOrig = vOrig.begin();
  std::vector<TNode*>::iterator itDest = vSort.begin();
  while (itOrig != vOrig.end()) {
    *itDest = CreateNode(*itOrig);
    itDest++;
    itOrig++;
  }

  // creamos el vector ordenado por la X
  std::sort(vSort.begin(), vSort.end(), CompareSortPointX);
  
  vTmp = _vectorSortX;
  _vectorSortX = new std::vector<TNode*>(vTmp->size() + vSort.size());
  std::merge(vTmp->begin(), vTmp->end(), vSort.begin(), vSort.end(), _vectorSortX->begin());
  delete vTmp;

  // creamos el vector ordenado por la Y
  std::sort(vSort.begin(), vSort.end(), CompareSortPointY);
  
  vTmp = _vectorSortY;
  _vectorSortY = new std::vector<TNode*>(vTmp->size() + vSort.size());
  std::merge(vTmp->begin(), vTmp->end(), vSort.begin(), vSort.end(), _vectorSortY->begin());
  delete vTmp;

  return true;
}

bool QuadTree::Clear( void )
{
  //CLEAR_VECTOR(TNode*, *_vectorSortX)
  ///CLEAR_VECTOR(TNode*, (*_vectorSortY))

  _vectorSortX->clear();
  _vectorSortY->clear();

  DeleteNode(_rootNode);
  _rootNode = NULL;

  return true;
}
 
int QuadTree::Load()
{
  // Try block to detect exceptions raised by any of the calls inside it
  try
  {
    // Turn off the auto-printing when failure occurs so that we can
    // handle the errors appropriately.
    H5::Exception::dontPrint();
    
    // Open group "QuadTree"
    H5::Group groupQT( _h5file->openGroup( H5_PATH_QUADTREE ) );

    // Call the recursive method
    _rootNode = LoadNodeGroup( groupQT );
   
    // Close the group
    groupQT.close();
  } // end of try block
  catch( H5::Exception& e ) 
  {
	  return -1;
  }

  return 0;
}

TNode* QuadTree::LoadNodeGroup( H5::Group &groupQT )
{
  TNode* result = new TNode;
  result->parent = NULL;
  // Try block to detect exceptions raised by any of the calls inside it
  try
  {
    // Turn off the auto-printing when failure occurs so that we can
    // handle the errors appropriately.
    H5::Exception::dontPrint();
    
    H5::Attribute attrPoint(groupQT.openAttribute( H5_CORNER_LOW_LEFT ));
    int point[2];
    attrPoint.read( H5::PredType::NATIVE_INT , &point);
    result->cornerLowLeft.x = point[0];
    result->cornerLowLeft.y = point[1];

    H5::Attribute attrWH(groupQT.openAttribute( H5_WIDTH_HEIGHT ));
    int wh[2];
    attrWH.read( H5::PredType::NATIVE_INT, &wh);
    result->width = wh[0];
    result->height = wh[1];

    H5::Attribute attrID(groupQT.openAttribute( H5_PAGE_NAME ));
    int id[2];
    attrID.read( H5::PredType::NATIVE_INT, &id);
    result->id = id[0];

    H5::Attribute attrCS(groupQT.openAttribute( H5_CELLSIZE ));
    int cs[2];
    attrCS.read( H5::PredType::NATIVE_INT, &cs);
    result->cellSize = cs[0];

    H5::Group grupoHijo;
    for ( int i = 0; i < 4 ; i++ ) {
      try{
        grupoHijo = H5::Group( groupQT.openGroup( std::to_string( i ) ) );
        result->childrens[i] = LoadNodeGroup( grupoHijo );
        if ( result->childrens[i] != NULL ) result->childrens[i]->parent = result;
      } // end of try block
      catch( H5::Exception& e ) {
        result->childrens[i] = NULL;
      }
    }
  } // end of try block
  catch( H5::Exception& e ) 
  {
  }

  return result;
}

int QuadTree::Save()
{
  // Try block to detect exceptions raised by any of the calls inside it
  try
  {
    // Turn off the auto-printing when failure occurs so that we can
    // handle the errors appropriately.
    H5::Exception::dontPrint();
    
    // Create group "QuadTree" and "Maps"     
    H5::Group groupQT( _h5file->createGroup( H5_PATH_QUADTREE ) );
    H5::Group groupMaps( _h5file->createGroup( H5_PATH_MAPS ) );

    // Call the recursive method
    CreateNodeGroup( _rootNode, groupQT, groupMaps );
   
    // Close the groups and file.
    groupQT.close();
    groupMaps.close();
  } // end of try block
  catch( H5::Exception& e ) 
  {
	  return -1;
  }

  return 0;
}

void QuadTree::CreateNodeGroup( const TNode* node, H5::Group &groupQT, H5::Group &groupMaps )
{
  // Try block to detect exceptions raised by any of the calls inside it
  try
  {
    // Turn off the auto-printing when failure occurs so that we can
    // handle the errors appropriately.
    H5::Exception::dontPrint();
    
	  // Create the attributes
    int attrCLL[2] = { node->cornerLowLeft.x , node->cornerLowLeft.y };
    hsize_t dimsCCL[1] = { 2 };
	  H5::DataSpace attrCLLDataspace( 1, dimsCCL );
    H5::Attribute attrCornerLowLeft = groupQT.createAttribute( H5_CORNER_LOW_LEFT, H5::PredType::STD_I32BE, attrCLLDataspace );
    attrCornerLowLeft.write( H5::PredType::NATIVE_INT, attrCLL );

    int attrWH[2] = { node->width, node->height };
    hsize_t dimsWH[1] = { 2 };
	  H5::DataSpace attrWHDataspace( 1, dimsWH );
    H5::Attribute attrWidthHeight = groupQT.createAttribute( H5_WIDTH_HEIGHT, H5::PredType::STD_I32BE, attrWHDataspace );
    attrWidthHeight.write( H5::PredType::NATIVE_INT, attrWH );

    int attrN[1] = {  node->value->GetID() };
    hsize_t dimsN[1] = { 1 };
	  H5::DataSpace attrNameDataspace( 1, dimsN );
    H5::Attribute attrName = groupQT.createAttribute( H5_PAGE_NAME, H5::PredType::STD_I32BE, attrNameDataspace );
    attrName.write( H5::PredType::NATIVE_INT, attrN );

    int attrCS[1] = { node->value->GetCellSize() };
    hsize_t dimsCS[1] = { 1 };
	  H5::DataSpace attrCellSizeDataspace( 1, dimsCS );
    H5::Attribute attrCellSize = groupQT.createAttribute( H5_CELLSIZE, H5::PredType::STD_I32BE, attrCellSizeDataspace );
    attrCellSize.write( H5::PredType::NATIVE_INT, attrCS );

    // create the group
    H5::Group* myGroup = node->value->CreateGroup( groupMaps );

    // refered bidirectionally the node of the quadtree with the page name
    groupQT.link( H5L_TYPE_SOFT, GetGroupName( *myGroup ), H5_LINK_PAGE );
    myGroup->link( H5L_TYPE_SOFT, GetGroupName( groupQT ), H5_LINK_PAGE );

    myGroup->close();
    delete myGroup;

    for ( int i = 0; i < 4 ; i++ ) {
      if ( node->childrens[i] != NULL ) {
        H5::Group groupChildren ( groupQT.createGroup( std::to_string( i ) ) );
        CreateNodeGroup( node->childrens[i], groupChildren, groupMaps );
        groupChildren.close();
      }
    }

  } // end of try block
  catch( H5::Exception& e ) 
  {
  }
}

void QuadTree::DeleteNode(TNode* node) {
  if (node == NULL) return;

  if (node->value != NULL) delete node->value;

  DeleteNode(node->childrens[0]);
  DeleteNode(node->childrens[1]);
  DeleteNode(node->childrens[2]);
  DeleteNode(node->childrens[3]);

  delete node;
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  QuadTree::Balance
 *  Description:  
 * =====================================================================================
 */
  void
QuadTree::Balance (  )
{
  _rootNode = Balance(_vectorSortX->begin(), _vectorSortX->end(), _vectorSortY->begin(), _vectorSortY->end());
}		/* -----  end of function QuadTree::Balance  ----- */

TNode* QuadTree::Balance(std::vector<TNode*>::iterator const &beginX, std::vector<TNode*>::iterator const &endX, 
  std::vector<TNode*>::iterator const &beginY, std::vector<TNode*>::iterator const &endY)
{
  if (endX - beginX <= 1) {
    return (endX - beginX == 1) ? (*beginX) : NULL;
  }

  // search the central virtual point
  TPoint* vp = VirtualPointFinder(beginX, endX, beginY, endY);

  // search the physical point nearest to the virtual point
  TPointDistance* pp = DistanciaMinima(beginX, endX, *vp);
  TNode* node = pp->node;
  delete pp;

  // free the virtual point
  delete vp;

  // find the point in the sorted vector x
  std::vector<TNode*>::iterator midX = std::lower_bound(beginX, endX, node->cornerLowLeft.x, CompareLowerPointIntX);

  // create the news vectors sorted in x
  std::vector<TNode*>* vectorSortX[4];
  SplitX(beginX, midX, node->cornerLowLeft.y, vectorSortX[2], vectorSortX[0]);
  SplitX(midX, endX, node, vectorSortX[3], vectorSortX[1]);

  // find the point in the sorted vector y
  std::vector<TNode*>::iterator midY = std::lower_bound(beginY, endY, node->cornerLowLeft.y, CompareLowerPointIntY);
  
  // create the news vectors sorted in y
  std::vector<TNode*>* vectorSortY[4];
  SplitY(beginY, midY, node->cornerLowLeft.x, vectorSortY[2], vectorSortY[3]);
  SplitY(midY, endY, node, vectorSortY[0], vectorSortY[1]);

  for( int i = 0; i < 4; i++ ) {
    node->childrens[i] = Balance(vectorSortX[i]->begin(), vectorSortX[i]->end(),
                                 vectorSortY[i]->begin(), vectorSortY[i]->end());

    delete vectorSortX[i];
    delete vectorSortY[i];
    if ( node->childrens[i] != NULL ) node->childrens[i]->parent = node;
  }

  return node;
}

void QuadTree::SplitX( std::vector<TNode*>::iterator const &a, std::vector<TNode*>::iterator const &b,
  int value, std::vector<TNode*>* &vectLow, std::vector<TNode*>* &vectHigh )
{
  std::list<TNode*>* listLow = new std::list<TNode*>;
  std::list<TNode*>* listHigh = new std::list<TNode*>;

  for( std::vector<TNode*>::iterator it = a; it != b; it++) {
    if ((*it)->cornerLowLeft.y < value) {
      listLow->push_back(*it);
    } else {
      listHigh->push_back(*it);
    }
  }

  vectLow = new std::vector<TNode*>(std::make_move_iterator(std::begin(*listLow)),
                                    std::make_move_iterator(std::end(*listLow)));

  delete listLow;

  vectHigh = new std::vector<TNode*>(std::make_move_iterator(std::begin(*listHigh)),
                                    std::make_move_iterator(std::end(*listHigh)));

  delete listHigh;
}

void QuadTree::SplitX( std::vector<TNode*>::iterator const &a, std::vector<TNode*>::iterator const &b,
  const TNode* node, std::vector<TNode*>* &vectLow, std::vector<TNode*>* &vectHigh )
{
  std::list<TNode*>* listLow = new std::list<TNode*>;
  std::list<TNode*>* listHigh = new std::list<TNode*>;

  std::vector<TNode*>::iterator it;
  for( it = a; *it != node; it++ ) {
    if ((*it)->cornerLowLeft.y < node->cornerLowLeft.y ) {
      listLow->push_back(*it);
    } else {
      listHigh->push_back(*it);
    }
  }

  if (it != b) {
    for( it++; it != b; it++ ) {
      if ((*it)->cornerLowLeft.y < node->cornerLowLeft.y ) {
        listLow->push_back(*it);
      } else {
        listHigh->push_back(*it);
      }
    }
  }

  vectLow = new std::vector<TNode*>(std::make_move_iterator(std::begin(*listLow)),
                                    std::make_move_iterator(std::end(*listLow)));

  delete listLow;

  vectHigh = new std::vector<TNode*>(std::make_move_iterator(std::begin(*listHigh)),
                                    std::make_move_iterator(std::end(*listHigh)));

  delete listHigh;
}


void QuadTree::SplitY( std::vector<TNode*>::iterator const &a, std::vector<TNode*>::iterator const &b,
  int value, std::vector<TNode*>* &vectLow, std::vector<TNode*>* &vectHigh )
{
  std::list<TNode*>* listLow = new std::list<TNode*>;
  std::list<TNode*>* listHigh = new std::list<TNode*>;

  for( std::vector<TNode*>::iterator it = a; it != b; it++) {
    if ((*it)->cornerLowLeft.x < value) {
      listLow->push_back(*it);
    } else {
      listHigh->push_back(*it);
    }
  }

  vectLow = new std::vector<TNode*>(std::make_move_iterator(std::begin(*listLow)),
                                    std::make_move_iterator(std::end(*listLow)));

  delete listLow;

  vectHigh = new std::vector<TNode*>(std::make_move_iterator(std::begin(*listHigh)),
                                    std::make_move_iterator(std::end(*listHigh)));

  delete listHigh;
}

void QuadTree::SplitY( std::vector<TNode*>::iterator const &a, std::vector<TNode*>::iterator const &b,
  const TNode* node, std::vector<TNode*>* &vectLow, std::vector<TNode*>* &vectHigh )
{
  std::list<TNode*>* listLow = new std::list<TNode*>;
  std::list<TNode*>* listHigh = new std::list<TNode*>;

  std::vector<TNode*>::iterator it;
  for( it = a; *it != node; it++ ) {
    if ((*it)->cornerLowLeft.x < node->cornerLowLeft.x ) {
      listLow->push_back(*it);
    } else {
      listHigh->push_back(*it);
    }
  }

  if (it != b) {
    for( it++; it != b; it++ ) {
      if ((*it)->cornerLowLeft.x < node->cornerLowLeft.x ) {
        listLow->push_back(*it);
      } else {
        listHigh->push_back(*it);
      }
    }
  }

  vectLow = new std::vector<TNode*>(std::make_move_iterator(std::begin(*listLow)),
                                    std::make_move_iterator(std::end(*listLow)));

  delete listLow;

  vectHigh = new std::vector<TNode*>(std::make_move_iterator(std::begin(*listHigh)),
                                    std::make_move_iterator(std::end(*listHigh)));

  delete listHigh;
}

TNode* QuadTree::CreateNode(IQuadTree* node) {
  TNode* myNode = new TNode;
  const TPoint* myPoint = node->GetCornerLowLeft();

  myNode->cornerLowLeft.x = myPoint->x;
  myNode->cornerLowLeft.y = myPoint->y;

  myNode->height = node->GetHeight();
  myNode->width = node->GetWidth();

  myNode->childrens[0] = NULL;
  myNode->childrens[1] = NULL;
  myNode->childrens[2] = NULL;
  myNode->childrens[3] = NULL;
  myNode->value = node;
 
  return myNode;
}


TPoint* QuadTree::VirtualPointFinder(std::vector<TNode*>::iterator const &beginX, std::vector<TNode*>::iterator const &endX, 
  std::vector<TNode*>::iterator const &beginY, std::vector<TNode*>::iterator const &endY) {
  // we go to search the virtual point with the min VTMR (Variance to Mean Radio)

  std::vector<TNode*>::iterator itX = beginX;
  std::vector<TNode*>::iterator itY = beginY;

  // the first indx is the quadrant and the second, the 0 define the number of points and the 1 the square of this number
  int pointQuadrant[4];
  pointQuadrant[0] = 0;
  pointQuadrant[1] = endX - beginX;
  pointQuadrant[2] = 0; 
  pointQuadrant[3] = 0; 

  std::vector<TNode*>::iterator itTmp;

  TPoint* vp = new TPoint;
  vp->x = (*itX)->cornerLowLeft.x; vp->y = (*itY)->cornerLowLeft.y;

  std::vector<TNode*>::iterator itOldX = itX;
  std::vector<TNode*>::iterator itOldY = itY;

  std::vector<TNode*>::iterator itFirstX = itX;
  std::vector<TNode*>::iterator itFirstY = itY;

  itX++; itY++;

  long long minValue = pointQuadrant[1]*pointQuadrant[1];
  long long minValueTmp = 0;

  int a1 = 0, a2 = 0, b1 = 0, b2 = 0;

  while (itX != endX) {
    // the real operation include the sum of the products by each quadrant
    // and how we know the old value of the quadrant only get que number of points
    // that change
     
    a1 = 0;
    for ( itTmp = itFirstY; (itTmp != endY) && ((*itTmp)->cornerLowLeft.y < (*itY)->cornerLowLeft.y); itTmp++ )
      if ( (*itTmp)->cornerLowLeft.x < (*itX)->cornerLowLeft.x ) a1++;

    a2 = ( itTmp - itFirstY ) - a1;

   
    b2 = 0;
    for ( itTmp = itFirstX; (itTmp != endX) && ((*itTmp)->cornerLowLeft.x < (*itX)->cornerLowLeft.x); itTmp++ )
      if ( (*itTmp)->cornerLowLeft.y < (*itFirstY)->cornerLowLeft.y ) b2++;

    b1 = ( itTmp - itFirstX ) - b2;

    if ((*itOldX)->cornerLowLeft.x != (*itX)->cornerLowLeft.x) itFirstX = itX;
    if ((*itOldY)->cornerLowLeft.y != (*itY)->cornerLowLeft.y) itFirstY = itY;

    pointQuadrant[0] += -a1 + b1;
    pointQuadrant[1] += -a2 - b1;
    pointQuadrant[2] +=  a1 + b2;
    pointQuadrant[3] +=  a2 - b2;
    minValueTmp = pointQuadrant[0]*pointQuadrant[0] +
                + pointQuadrant[1]*pointQuadrant[1] +
                + pointQuadrant[2]*pointQuadrant[2] +
                + pointQuadrant[3]*pointQuadrant[3];

    if ( minValueTmp < minValue ) {
      minValue = minValueTmp;
      vp->x = (*itX)->cornerLowLeft.x; vp->y = (*itY)->cornerLowLeft.y;
    }
    
    itX++; itY++; itOldX++; itOldY++;
  }

  return vp;
}

TPointDistance* QuadTree::DistanciaMinima(std::vector<TNode*>::iterator const &a, std::vector<TNode*>::iterator const &b, TPoint const& p)
{
  int distance = b-a;
  if (distance > 1) {
    std::vector<TNode*>::iterator mid = a;
    std::advance(mid, (distance>>1));

    TPointDistance* resultado = NULL;
    TPointDistance* resultadoError = NULL;
        
    // buscamos la distancia minima en el sector donde este el punto
    if (p.x <= (*mid)->cornerLowLeft.x) {
      // buscamos en la partición mas cercana
      resultado = DistanciaMinima(a, mid, p);
      
      // buscamos en la franja en la que puede haber un valor menor al obtenido
      std::vector<TNode*>::iterator midError = std::upper_bound(mid, b, p.x + resultado->distance, CompareUpperPointDoubleX );

      // buscamos en dicha franja el valor mas pequeño
      // para ello primero comprobamos si los extremos han cambiado
      resultadoError = ((mid != a) && (midError != b)) ? DistanciaMinima(mid, midError, p) : DistanciaMinimaFB(mid, midError, p);
    } else {
      // buscamos en la partición mas cercana
      resultado = DistanciaMinima(mid, b, p);
      
      // buscamos en la franja en la que puede haber un valor menor al obtenido
      std::vector<TNode*>::iterator midError = std::lower_bound(a, mid, p.x - resultado->distance, CompareLowerPointDoubleX );

      // buscamos en dicha franja el valor mas pequeño
      // para ello primero comprobamos si los extremos han cambiado
      resultadoError = ((midError != a) && (mid != b)) ? DistanciaMinima(midError, mid, p) : DistanciaMinimaFB(midError, mid, p);
    }

      // si hemos encontrado un punto mas cercano lo definimos como resultado
      if (resultado->distance < resultadoError->distance) {
        delete resultadoError;
        return resultado;
      } else {
        delete resultado;
        return resultadoError;
      }
  } else {
    TPointDistance* resultado = new TPointDistance;

    if (distance == 1) {
      resultado->node = *a;
      resultado->distance = PITAGORAS((*a)->cornerLowLeft.x - p.x, (*a)->cornerLowLeft.y - p.y);
    } else {
      resultado->node = NULL;
      resultado->distance = std::numeric_limits<float>::max();
    }

    return resultado;
  }
}


TPointDistance* QuadTree::DistanciaMinimaFB(std::vector<TNode*>::iterator const &a, std::vector<TNode*>::iterator const &b, TPoint const& p)
{
  std::vector<TNode*>::iterator it;
  TPointDistance* resultado = new TPointDistance;
  resultado->node = NULL;
  resultado->distance = std::numeric_limits<float>::max();
  
  long long d;
  for (it = a; it!=b; it++) {
    d = POW2((*it)->cornerLowLeft.x - p.x, (*it)->cornerLowLeft.y - p.y);
    if (d < resultado->distance) {
      resultado->distance = d;
      resultado->node = *it;
    }
  }

  resultado->distance = sqrtf(resultado->distance);
  return resultado;
}
