/*
 * =====================================================================================
 *
 *       Filename:  splicemap.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  21/09/14 14:29:29
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Juan Manuel (), JuanManuel.Fernandez5@alu.uclm.es
 *   Organization:  
 *
 * =====================================================================================
 */

#include "splicemap.h"

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  SpliceMap::SpliceMap
 *  Description:  
 * =====================================================================================
 */
 
SpliceMap::SpliceMap ( void )
{
  _numCols = 0;
  _numRows = 0;
  _cornerLowLeft.x = 0;
  _cornerLowLeft.y = 0;
  _cellSize = 0;
  _noDataValue = 0;
  _values = NULL;
  _id = 0;
}	

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  SpliceMap::SpliceMap
 *  Description:  
 * =====================================================================================
 */
 
SpliceMap::~SpliceMap ( void )
{
  if ( _values != NULL ) delete _values;
}

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  SpliceMap::ReadHeader
 *  Description:  
 * =====================================================================================
 */
  bool
SpliceMap::ReadHeader (  )
{
  // Open the zip file
  char *txtfile = Unzip( );
  if ( txtfile == NULL ) return false;

  int ncols, nrows, cellSize;

  // #checkme: if whe had other head format all go good
  // read the values and check if all his read
  bool correctly = sscanf( txtfile, "NCOLS %d NROWS %d XLLCENTER %d YLLCENTER %d CELLSIZE %d NODATA_VALUE %d",
    &ncols, &nrows, &_cornerLowLeft.x, &_cornerLowLeft.y, &cellSize, &_noDataValue ) == 6; 

  NumCols(ncols);
  NumRows(nrows);
  CellSize(cellSize);

  //_cornerLowLeft.y -= GetHeight();
  _cornerLowLeft.x -= GetWidth();
  
    // free the memory
  free( txtfile );

  return correctly;
}		/* -----  end of function SpliceMap::ReadHeader  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  SpliceMap::ReadValues
 *  Description:  
 * =====================================================================================
 */
  bool
SpliceMap::ReadValues (  )
{
  // Open the zip file
  char *txtfile = Unzip();
  if ( txtfile == NULL ) return false;

  // reserve the memory of the table ( first x, second y )
  _values = new double[_numCols * _numRows];

  std::string myString = txtfile;
  strtk::multiple_char_delimiter_predicate predicate(" \r\n");
  typedef strtk::std_string::tokenizer<strtk::multiple_char_delimiter_predicate>::type tokenizer_type;
  tokenizer_type tokenizer( myString, predicate,strtk::tokenize_options::compress_delimiters );
  tokenizer_type::iterator itr = tokenizer.begin();

  itr += 12; // salt the head
  int indx = _numCols * (_numRows-1), indxMax = _numCols * _numRows, numCols2 = _numCols<<1;

  // get the table values
  while ( (indx >= 0) && (tokenizer.end() != itr) ) {
    while ( (indx < indxMax) && (tokenizer.end() != itr) ) {
      try {
        _values[indx] = boost::lexical_cast<double>( itr.as_string() );
      }  catch ( boost::bad_lexical_cast & ) { // if had a error define the defauld value
        _values[indx] = _noDataValue;
      }

      itr++;
      indx++;
    }
    indx -= numCols2;
    indxMax -= _numCols;
  }

  free( txtfile );

  return indxMax == 0;
}		/* -----  end of function SpliceMap::ReadValues  ----- */



/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  SpliceMap::Unzip
 *  Description:  
 * =====================================================================================
 */
  char *
SpliceMap::Unzip ( )
{
  int err;
  struct zip *z;
  struct zip_file *zf;
  struct zip_stat st;
  char *contents;
  
  z=zip_open(_fileDir.c_str() , 0, &err);
  
  if (z == NULL) return NULL;

	int numberOfFiles = zip_get_num_files(z);
  if ( numberOfFiles <= 0 ) {
    zip_close(z);
    return NULL;
  }

  // get the file metadata
  zip_stat_init( &st );
  zip_stat_index( z, 0 /* indx */, 0, &st );

  // allow memory for uncompressed contents
  contents = (char*) malloc( st.size );
  if ( contents == NULL ) {
    zip_close(z);
    return NULL;
  }

  // read the file
	zf = zip_fopen_index(z, 0 /* indx */, 0);
  zip_fread( zf, contents, st.size );

  // close the file in the zip
	zip_fclose(zf);
	
  // Close this zip file
	zip_close(z);

  return contents;
}		/* -----  end of function SpliceMap::Unzip  ----- */


bool 
SpliceMap::ObtainPageIdZip (  )
{
  int err;
  struct zip *z;
  struct zip_stat st;
  
  z=zip_open(_fileDir.c_str() , 0, &err);
  
  if (z == NULL) return false;

	int numberOfFiles = zip_get_num_files(z);
  if ( numberOfFiles <= 0 ) {
    zip_close(z);
    return false;
  }

  // get the file metadata
  zip_stat_init( &st );
  zip_stat_index( z, 0 /* indx */, 0, &st );

  std::string myString = st.name;
  strtk::multiple_char_delimiter_predicate predicate("-");
  typedef strtk::std_string::tokenizer<strtk::multiple_char_delimiter_predicate>::type tokenizer_type;
  tokenizer_type tokenizer( myString, predicate,strtk::tokenize_options::compress_delimiters );
  tokenizer_type::iterator itr = tokenizer.begin();

  itr += 1; // change the item to the page name

  // Get the name of the title
  _id = boost::lexical_cast<int>( itr.as_string() );
	
  // Close this zip file
	zip_close(z);

  return true;
}		/* -----  end of function SpliceMap::ObtainPageNameZip  ----- */




/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  SpliceMap::GetCornerLowLeft
 *  Description:  
 * =====================================================================================
 */
 const TPoint*
SpliceMap::GetCornerLowLeft ()
{
  return &_cornerLowLeft;
}		/* -----  end of function SpliceMap::GetCornerLowLeft  ----- */

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  SpliceMap::GetWidth
 *  Description:  
 * =====================================================================================
 */
  int
SpliceMap::GetWidth ()
{
  return _width;
}		/* -----  end of function SpliceMap::GetWidth  ----- */


/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  SpliceMap::GetHeight
 *  Description:  
 * =====================================================================================
 */
  int
SpliceMap::GetHeight (  )
{
  return _height;
}		/* -----  end of function SpliceMap::GetHeight  ----- */


int SpliceMap::GetCellSize() {
  return _cellSize;
}

double* SpliceMap::GetData() const {
  return _values;
}

double SpliceMap::GetData(int x, int y) {
  return _values[ y*_numCols + x ];
}


int SpliceMap::GetID() const {
  return _id;
}

bool SpliceMap::ReadGroup( const H5::Group& group ) {
  bool allFine = false;

  try
  {
    // Turn off the auto-printing when failure occurs so that we can
    // handle the errors appropriately.
    H5::Exception::dontPrint();

    H5::DataSet dataset(group.openDataSet( H5_PAGE_VALUES ) );

    H5::Attribute attrPoint(group.openAttribute( H5_CORNER_LOW_LEFT ));
    int point[2];
    attrPoint.read( H5::PredType::NATIVE_INT , &point);
    _cornerLowLeft.x = point[0];
    _cornerLowLeft.y = point[1];

    H5::Attribute attrCellSize(group.openAttribute( H5_CELLSIZE ));
    int cellSize[1];
    attrCellSize.read( H5::PredType::NATIVE_INT, &cellSize);
    _cellSize = cellSize[0];

    H5::Attribute attrID(group.openAttribute( H5_PAGE_NAME ));
    int id[1];
    attrID.read( H5::PredType::NATIVE_INT, &id);
    _id = id[0];

    /*
     * Get filespace for rank and dimension
     */
    H5::DataSpace dataspace = dataset.getSpace();

    /*
     * Get the dimension sizes of the file dataspace
     */
    hsize_t dims[2]; 	// dataset dimensions
    dataspace.getSimpleExtentDims( dims );
    NumRows( dims[0] );
    NumCols( dims[1] );

    /*
     * Select hyperslab for the dataset in the file, using 1x1 blocks,
     */
    hsize_t offset[2]; // Start of hyperslab
    offset[0] = 0;
    offset[1] = 0;

    dataspace.selectHyperslab( H5S_SELECT_SET, dims, offset);

    H5::DataSpace mspace(2, dims);

    if ( _values != NULL ) delete _values;
    _values = new double[_numCols * _numRows];
    dataset.read( _values , H5::PredType::NATIVE_DOUBLE, mspace, dataspace);
    
    allFine = true;
    dataset.close();
  }
  catch( H5::Exception& e) 
  {
  }

  return allFine;
}

H5::Group* SpliceMap::CreateGroup(const H5::Group& parentGroup) {
  H5::Group* myGroup = NULL;

  ReadValues();
  
  // Try block to detect exceptions raised by any of the calls inside it
  try
  {
    // Turn off the auto-printing when failure occurs so that we can
    // handle the errors appropriately.
    H5::Exception::dontPrint();
    
    myGroup = new H5::Group( parentGroup.createGroup( std::to_string( _id ) ) );

	  // Create the attributes
    int attrCLL[2] = { _cornerLowLeft.x , _cornerLowLeft.y };
    hsize_t dims[1] = { 2 };
	  H5::DataSpace* attrCLLDataspace = new H5::DataSpace ( 1, dims );
    H5::Attribute attrCornerLowLeft = myGroup->createAttribute( H5_CORNER_LOW_LEFT, H5::PredType::STD_I32BE, *attrCLLDataspace );
    attrCornerLowLeft.write( H5::PredType::NATIVE_INT, attrCLL );

    int attrCS[1] = { _cellSize };
    hsize_t dims2[1] = { 1 };
	  H5::DataSpace* attrCellSizeDataspace = new H5::DataSpace ( 1, dims2 );
    H5::Attribute attrCellSize = myGroup->createAttribute( H5_CELLSIZE, H5::PredType::STD_I32BE, *attrCellSizeDataspace );
    attrCellSize.write( H5::PredType::NATIVE_INT, attrCS );

    int attrIDValue[1] = { _id };
    hsize_t dims3[1] = { 1 };
	  H5::DataSpace* attrIdDataspace = new H5::DataSpace ( 1, dims3 );
    H5::Attribute attrID = myGroup->createAttribute( H5_PAGE_NAME, H5::PredType::STD_I32BE, *attrIdDataspace );
    attrID.write( H5::PredType::NATIVE_INT, attrIDValue );

    hsize_t dimsf[2];              // dataset dimensions
    dimsf[0] = _numRows;
    dimsf[1] = _numCols;
    H5::DataSpace dataspace( 2, dimsf );

    // compres the data with a level 6 (MAX)
    H5::DSetCreatPropList ds_creatplist; // create dataset creation prop list
    ds_creatplist.setChunk( 2, dimsf ); // then modify it for compression
    ds_creatplist.setDeflate( 6 );

    /*
     * Create a new dataset within the file using defined dataspace and
     * datatype and default dataset creation properties.
     */
    H5::DataSet dataset = myGroup->createDataSet( H5_PAGE_VALUES, H5::PredType::IEEE_F64BE, dataspace, ds_creatplist );

    /*
     * Write the data to the dataset using default memory space, file
     * space, and transfer properties.
     */
    dataset.write( _values, H5::PredType::NATIVE_DOUBLE );    
  } // end of try block
  catch( H5::Exception& e ) 
  {
	 myGroup = NULL;
  }

  //for ( int i = 0; i < _numCols; i++ ) delete _values[i];

  return myGroup;
}


