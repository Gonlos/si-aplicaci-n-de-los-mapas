/*
 * =====================================================================================
 *
 *       Filename:  frontier.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  28/10/14 03:56:02
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Juan Manuel (), JuanManuel.Fernandez5@alu.uclm.es
 *   Organization:  
 *
 * =====================================================================================
 */

#include "frontier.h"

Frontier::Frontier( bool poda, std::string& estrategia ) {
  _hashTable = kh_init(longlong);
  _priorityQueue = new std::priority_queue< FrontierNode*, std::vector<FrontierNode*>, LessFrontierNode >;

  if ( poda ) {
    if ( ( estrategia == "profundidad" ) || ( estrategia == "anchura" ) ) {
      _trimState = TRIM_ON_VISITED;
    } else {
      _trimState = TRIM_ON_LOW_COST;
    }
  } else {
    _trimState = TRIM_OFF;
  }
}

Frontier::~Frontier() {
  khiter_t k;
  HashListMovements *hashMovements;
  HashListFrontierNode *hashNode, *oldHashNode;
  for (k = kh_begin( _hashTable ); k != kh_end( _hashTable ); k++)
    if (kh_exist( _hashTable , k)) {
      hashMovements = kh_value( _hashTable, k);
      for ( int i = 0; i < 9; i++ ) {
        hashNode = hashMovements->movement[i]; 
        while ( hashNode != NULL ) {
          if ( hashNode->value->estado != NULL ) delete hashNode->value->estado;
          delete hashNode->value;
          oldHashNode = hashNode;
          hashNode = hashNode->next;
          delete oldHashNode;
        }
      }
      delete hashMovements;
    }

  kh_destroy(longlong, _hashTable);
  delete _priorityQueue;
}

int Frontier::Add( FrontierNode* myNode ) {
  int returnCode;
  long long id = myNode->estado->GetID();

  khiter_t k = kh_get(longlong, _hashTable, id);
  if (k == kh_end( _hashTable )) {
    k = kh_put(longlong, _hashTable, id, &returnCode);

    HashListMovements* myMove = new HashListMovements;

    for ( int i = 0; i < 9; i++ )
      myMove->movement[i] = ( i == myNode->action ) ? new HashListFrontierNode( { myNode, NULL } ) : NULL;
    
    kh_val(_hashTable, k) = myMove;
  } else {
    if ( kh_val(_hashTable, k)->movement[ myNode->action ] != NULL ) {
      HashListFrontierNode* myList = kh_val(_hashTable, k)->movement[ myNode->action ]; 

      // check if we ned the trim
      switch ( _trimState ) {
        case TRIM_OFF: // OFF, add all the new nodes
          break;
        case TRIM_ON_VISITED: // ON_VISITED, if we are here is because before we added a node
          return -1;
        case TRIM_ON_LOW_COST: // ON_LOW_COST, add the node if it have less assessment
          if ( myList->value->cost <= myNode->cost ) return -1;
          break;
      }
    
      kh_val(_hashTable, k)->movement[ myNode->action ] = new HashListFrontierNode( { myNode, myList } );
    } else {
      kh_val(_hashTable, k)->movement[ myNode->action ] = new HashListFrontierNode( { myNode, NULL } );
    }
  }

  _priorityQueue->push( myNode );

  return 0;
}

FrontierNode* Frontier::PopFirst() {
  if ( _priorityQueue->empty() ) return NULL;
  FrontierNode* myNode = _priorityQueue->top();
  _priorityQueue->pop();

  return myNode;
}
