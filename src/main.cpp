/*
 * =====================================================================================
 *
 *       Filename:  main.cpp
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  21/09/14 14:21:19
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Juan Manuel (), JuanManuel.Fernandez5@alu.uclm.es
 *   Organization:  
 *
 * =====================================================================================
 */


#include	<stdlib.h>
#include "splicemap.h"
#include "quadtree.h"
#include "virtualmap.h"
#include "state.h"
#include <string>

/* 
 * ===  FUNCTION  ======================================================================
 *         Name:  main
 *  Description:  
 * =====================================================================================
 */

void deleteSolution( TSolution* solution );
void saveSolutionTo( std::string fileName, long time, TSolution* solution );
void help();

int main ( int argc, char *argv[] ) {
  srand( time( NULL ) );

  if (argc < 2) {
    help();
    return EXIT_SUCCESS;
  } else {
    if ( strcmp("create", argv[1]) == 0 ) {
      VirtualMap vp( H5_FILE_NAME );
      vp.Init( argv+2 );
    } else if ( strcmp("lg", argv[1]) == 0) {
      TLugar* l;
      VirtualMap vp( H5_FILE_NAME );
      vp.ReadFile();
      vp.LoadData();
      l = vp.LugarGeografico( boost::lexical_cast<int>( argv[2] ), 
                              boost::lexical_cast<int>( argv[3] ) );

      if (l == NULL) {
        std::cout << "No existe dicha hoja" << std::endl;
        return EXIT_FAILURE;
      } else {
        std::cout << "Hoja: " << l->hoja << std::endl;
        std::cout << "X: " << l->x << "   Y: " << l->y << std::endl;
        return EXIT_SUCCESS;
      }
    } else if ( strcmp("al", argv[1]) == 0) {
      TLugar l;
      l.hoja = boost::lexical_cast<int>( argv[2] );
      l.x = boost::lexical_cast<int>( argv[3] );
      l.y = boost::lexical_cast<int>( argv[4] );

      VirtualMap vp( H5_FILE_NAME );
      vp.ReadFile();
      vp.LoadData();
      double altitud;
      bool error;

      altitud = vp.Altitud( l, error );

      if ( error ) {
        std::cout << "Los datos introducidos son erroneos" << std::endl;
        return EXIT_FAILURE;
      } else {
        std::cout << "Altitud: " << altitud << std::endl;
        return EXIT_SUCCESS;
      }
    } else if ( strcmp("todo", argv[1]) == 0) {
      TLugar* l;
      VirtualMap vp( H5_FILE_NAME );
      vp.ReadFile();
      vp.LoadData();
      l = vp.LugarGeografico( boost::lexical_cast<int>( argv[2] ), 
                              boost::lexical_cast<int>( argv[3] ) );
      if (l == NULL) {
        std::cout << "No existe dicha hoja" << std::endl;
        return EXIT_FAILURE;
      } else {
        
        double altitud;
        bool error;

        altitud = vp.Altitud( *l, error );

        if ( error ) {
          std::cout << "Los datos introducidos son erroneos" << std::endl;
          return EXIT_FAILURE;
        } else {
          std::cout << "Hoja: " << l->hoja << std::endl;
          std::cout << "X: " << l->x << "   Y: " << l->y << std::endl;
          std::cout << "Altitud: " << altitud << std::endl;
          return EXIT_SUCCESS;
        }
      }
    } else if ( strcmp("ss", argv[1]) == 0) {
      TLugar *l = new TLugar;
      l->hoja = boost::lexical_cast<int>( argv[2] );
      l->x = boost::lexical_cast<int>( argv[3] );
      l->y = boost::lexical_cast<int>( argv[4] );

      State myState;
      myState.Lugar( l );

      VirtualMap vp( H5_FILE_NAME );
      vp.ReadFile();
      vp.LoadData();

      int utmX, utmY;
      bool error;
      vp.UTM( *l, utmX, utmY, error );
      myState.UTM( new TPoint { utmX, utmY } );

      TSuccessorAndState mySuccessors[ LENGTH_PLACES_X * LENGTH_PLACES_Y ];
      int n = vp.Successors( &myState, &mySuccessors[0] );

      if ( n >= 0 ) {
        //std::cout << "Se han encontrado " << myList->size() << " sucesores" << std::endl;
        for ( int i = 0; i < n; i++ ) {
          std::cout << mySuccessors[ i ].state->GetText() << std::endl;
          delete mySuccessors[ i ].state;
        }
      } else {
        std::cout << "Los datos introducidos son erroneos" << std::endl;
      }

      return EXIT_SUCCESS;
    } else if ( strcmp("search", argv[1]) == 0) {
      TLugar *l = new TLugar;
      l->hoja = boost::lexical_cast<int>( argv[2] );
      l->x = boost::lexical_cast<int>( argv[3] );
      l->y = boost::lexical_cast<int>( argv[4] );

      TLugar *l2 = new TLugar;
      l2->hoja = boost::lexical_cast<int>( argv[5] );
      l2->x = boost::lexical_cast<int>( argv[6] );
      l2->y = boost::lexical_cast<int>( argv[7] );

      int maxDepth = boost::lexical_cast<int>( argv[8] );
      std::string estrategia = argv[9];
      
      bool poda = 'y' == *argv[10];

      State* myState = new State;
      myState->Lugar( l );
      myState->Cellsize( 25 ); // checkme!

      State* myState2 = new State;
      myState2->Lugar( l2 );
      myState2->Cellsize( 25 );

      VirtualMap vp( H5_FILE_NAME );
      vp.ReadFile();
      vp.LoadData();

      int utmX, utmY;
      bool error;
      vp.UTM( *l, utmX, utmY, error );
      if ( error ) {
        std::cout << "ERROR: El lugar de inicio no existe." << std::endl;
        return EXIT_FAILURE;
      }
      
      myState->UTM( new TPoint { utmX, utmY } );

      myState->Altitud( vp.Altitud( *l, error ) );
      
      vp.UTM( *l2, utmX, utmY, error );
      if ( error ) {
        std::cout << "ERROR: El lugar de destino no existe." << std::endl;
        return EXIT_FAILURE;
      }
      
      myState2->UTM( new TPoint { utmX, utmY } );

      vp.InitialState( myState );
      vp.FinalState( myState2 );


      long millis;
      timeb time1, time2;

      ftime(&time1);
      TSolution* solution = vp.SearchSolution( maxDepth, estrategia, poda );
      ftime(&time2);

      millis = ( time2.millitm + (time2.time & 0xfffff) * 1000 ) - ( time1.millitm + (time1.time & 0xfffff) * 1000 );
      

      std::cout << "¿Se ha encontrado la solución?: " << ( solution->depth > 0 ? "Si" : "No") << std::endl;

      if ( solution->depth > 0  ) {
        std::cout << "Numero de elementos de la solución: " << std::to_string( solution->depth ) << std::endl;
        std::cout << "Coste de la solución: " << std::to_string( solution->cost ) << std::endl;
      }

      std::cout << "Tiempo total: " << std::to_string( millis ) << "ms" << std::endl;
      
      if ( solution->depth > 0 ) {
        State* firstState = solution->path[ 0 ];
        State* finalState = solution->path[ solution->depth - 1 ];

        std::string fileName = "Sol de [ " + std::to_string( firstState->UTM()->x ) +
                                 " , " + std::to_string( firstState->UTM()->y ) + 
                                 " ] a [ " + std::to_string( finalState->UTM()->x ) +
                                 " , " + std::to_string( finalState->UTM()->y ) +
                                 " ] con profundidad maxima ( " + std::to_string( maxDepth ) + 
                                 " ) y estrategia ( " + estrategia + " )";
        

        std::cout << "Generando el archivo de solucion '" + fileName + "'" << std::endl;
        saveSolutionTo( fileName, millis, solution );
        std::cout << "Creado el archivo" << std::endl;
        deleteSolution( solution );
      } 

      delete myState2;

      return EXIT_SUCCESS;
    } else {
     help();
    }
  }
}				/* ----------  end of function main  ---------- */

void deleteSolution( TSolution* solution ) {
  for ( int i = 0; i < solution->depth; i++ )
    delete solution->path[ i ];
  
  delete solution;
}

void saveSolutionTo( std::string fileName, long time, TSolution* solution ) {
  std::ofstream outfile ( fileName );

  outfile << "Coste: ( " << std::to_string( solution->cost ) << " ), Profundidad: ( " <<
             std::to_string( solution->depth ) << " ), Tiempo empleado: ( " <<
             std::to_string( time ) << "ms )"<< std::endl;
  
  for ( int i = 0; i < solution->depth; i++ ) 
    outfile << solution->path[ i ]->GetText() << std::endl;
    
  outfile.close();
}

void help() {
  std::cout << "Debe incluir el modo:" <<std::endl;
  std::cout << "  create: crear el fichero HDF5" <<std::endl;
  std::cout << "  lg: muestra los datos de las cordenadas UTM introducidas (x, y)" <<std::endl;
  std::cout << "  al: muestra la altitud (hoja, columna, fila)" <<std::endl;
  std::cout << "  todo: muestra los datos y la altitud de unas coordenadas UTM" <<std::endl;
  std::cout << "  ss: muestra los sucesores indicando los datos ( pagina, columna, fila )" <<std::endl;
  std::cout << "  search: busca la solucion de ( pagina-inicio, columna-inicio, fila-inicio, pagina-final, columna-final, fila-final, profundidad máxima, estrategia a utilizar )" <<std::endl;
}

