/*
 * =====================================================================================
 *
 *       Filename:  quadtree.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  24/09/14 02:26:06
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Juan Manuel (), JuanManuel.Fernandez5@alu.uclm.es
 *   Organization:  
 *
 * =====================================================================================
 */

#include "splicemap.h"

#ifndef __QUADTREE__
#define __QUADTREE__

#include "structures.h"
#include <iostream>     // std::cout
#include <algorithm>    // std::sort
#include <vector>       // std::vector
#include <stack>       // std::stack
#include <list>
#include <iterator>     // std::make_move_iterator
#include <math.h>
#include <limits>

#include <time.h>
#include <stdlib.h>
#include <stdio.h>

#include <state.h>

#include "H5Cpp.h"

class IQuadTree;
class QuadTree;

struct TNode;
typedef struct TNode TNode;

struct TNode {
  TNode* parent;
  TNode* childrens[4];
  TPoint cornerLowLeft;
  int cellSize;
  int id;
  int width;
  int height;
  IQuadTree* value;
};

struct TPointDistance;
typedef struct TPointDistance TPointDistance;

struct TPointDistance {
  TNode* node;
  float distance;
};


struct TResultNeighbor {
  TLugar* lugar;
  TState action;
};				/* ----------  end of struct TResultNeighbor  ---------- */

typedef struct TResultNeighbor TResultNeighbor;

struct TNeighbors {
  TLugar* places[LENGTH_PLACES_Y][LENGTH_PLACES_X];
  TPoint cornerLowLeftUTM;
  int cellSize;
  int remaining;
};				/* ----------  end of struct TSuccessors  ---------- */

typedef struct TNeighbors TNeighbors;

/*
 * =====================================================================================
 *        Class:  IQuadTree
 *  Description:  Interface to use the QuadTree class
 * =====================================================================================
 */
class IQuadTree
{
  public:
    /* ====================  LIFECYCLE     ======================================= */
    virtual ~IQuadTree () {}                             /* destructor */

    virtual const TPoint* GetCornerLowLeft() = 0;
    virtual int GetWidth() = 0;
    virtual int GetHeight() = 0;
    virtual double* GetData() const = 0;
    virtual int GetID() const = 0;
    virtual int GetCellSize() = 0;
    virtual H5::Group* CreateGroup(const H5::Group &parentGroup) = 0;

    TNode* GetQTNode() const { return _qtNode; }
    void SetQTNOde(TNode* value) { _qtNode = value; }

    /* ====================  ACCESSORS     ======================================= */

    /* ====================  MUTATORS      ======================================= */

    /* ====================  OPERATORS     ======================================= */

  protected:
    /* ====================  METHODS       ======================================= */

    /* ====================  DATA MEMBERS  ======================================= */

  private:
    /* ====================  METHODS       ======================================= */

    /* ====================  DATA MEMBERS  ======================================= */
    TNode* _qtNode;

}; /* -----  end of class IQuadTree  ----- */


/*
 * =====================================================================================
 *        Class:  QuadTree
 *  Description:  
 * =====================================================================================
 */
class QuadTree
{
  public:
    /* ====================  LIFECYCLE     ======================================= */
    QuadTree();                             /* constructor */
    ~QuadTree();                             /* constructor */

    /* ====================  ACCESSORS     ======================================= */


    bool Add(std::vector<IQuadTree*> &vOrig );
    bool Clear();
    int Save();
    int Load();
    void Balance();

    TLugar* LugarGeografico(int utmX, int utmY);
    void SetVirtualMap(IQuadTree* sm, const std::string& groupDir);
    int Neighbors( int utmX, int utmY, IQuadTree* map, TResultNeighbor* result );

    //std::string FileDir() const { return _fileDir; }
    //void FileDir(std::string value) { _fileDir = value; }
    void H5File(H5::H5File* value) { _h5file = value; }

    /* ====================  MUTATORS      ======================================= */

    /* ====================  OPERATORS     ======================================= */

  protected:
    /* ====================  METHODS       ======================================= */

    /* ====================  DATA MEMBERS  ======================================= */

  private:
    /* ====================  METHODS       ======================================= */


    bool InitStructNeighbors( int utmX, int utmY, int cellSize, TNeighbors* result );
    void SearchNeighbors( TNode* firstNode, TNeighbors* result );

    std::string GetGroupName(const H5::Group& ds);

    TNode* CreateNode(IQuadTree* mnode);
    void DeleteNode(TNode* node);

    TNode* Balance(std::vector<TNode*>::iterator const &beginX, std::vector<TNode*>::iterator const &endX, 
      std::vector<TNode*>::iterator const &beginY, std::vector<TNode*>::iterator const &endY);

    void CreateNodeGroup( const TNode* parentNode, H5::Group &parentGroup, H5::Group &groupMaps );
    TNode* LoadNodeGroup( H5::Group &groupQT );

    void SplitX( std::vector<TNode*>::iterator const &a, std::vector<TNode*>::iterator const &b,
      int value, std::vector<TNode*>* &vectLow, std::vector<TNode*>* &vectHigh );

    void SplitY( std::vector<TNode*>::iterator const &a, std::vector<TNode*>::iterator const &b,
      int value, std::vector<TNode*>* &vectLow, std::vector<TNode*>* &vectHigh );


    void SplitX( std::vector<TNode*>::iterator const &a, std::vector<TNode*>::iterator const &b,
      const TNode* node, std::vector<TNode*>* &vectLow, std::vector<TNode*>* &vectHigh );

    void SplitY( std::vector<TNode*>::iterator const &a, std::vector<TNode*>::iterator const &b,
      const TNode* node, std::vector<TNode*>* &vectLow, std::vector<TNode*>* &vectHigh );


    TPointDistance* DistanciaMinima(std::vector<TNode*>::iterator const &a, std::vector<TNode*>::iterator const &b, TPoint const& p);
    TPointDistance* DistanciaMinimaFB(std::vector<TNode*>::iterator const &a, std::vector<TNode*>::iterator const &b, TPoint const& p);

    TPoint* VirtualPointFinder(std::vector<TNode*>::iterator const &beginX, std::vector<TNode*>::iterator const &endX, 
      std::vector<TNode*>::iterator const &beginY, std::vector<TNode*>::iterator const &endY);
    
    /* ====================  DATA MEMBERS  ======================================= */
    TNode *_rootNode;

    std::vector<TNode*>* _vectorSortX;
    std::vector<TNode*>* _vectorSortY;

    // related with the file
    //std::string _fileDir;
    H5::H5File* _h5file;

}; /* -----  end of class QuadTree  ----- */

#endif
