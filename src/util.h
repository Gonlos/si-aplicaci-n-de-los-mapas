/*
 * =====================================================================================
 *
 *       Filename:  util.h
 *
 *    Description:  
 *
 *        Version:  1.0
 *        Created:  21/09/14 14:29:29
 *       Revision:  none
 *       Compiler:  gcc
 *
 *         Author:  Juan Manuel (), JuanManuel.Fernandez5@alu.uclm.es
 *   Organization:  
 *
 * =====================================================================================
 */

#ifndef __SPLICEMAP__
#define __SPLICEMAP__

#include <stdlib.h>
#include <stdio.h>
#include <string>
#include <boost/lexical_cast.hpp>
#include <strtk.hpp>
#include <zip.h>
#include <cerrno> // allow usage of errno

#include "structures.h"
#include "quadtree.h"

#include <boost/spirit/include/qi_parse.hpp>
#include <boost/spirit/include/qi_numeric.hpp>

class SpliceMap;

/*
 * =====================================================================================
 *        Class:  SpliceMap
 *  Description:  Class that allow us manage the parcial maps
 * =====================================================================================
 */
class SpliceMap: public IQuadTree
{
  public:
    /* ====================  LIFECYCLE     ======================================= */
    SpliceMap ();                             /* constructor */
    ~SpliceMap();

    /* ====================  ACCESSORS     ======================================= */

    bool ReadHeader();
    bool ReadValues();

    std::string FileDir() const { return _fileDir; }
    void FileDir(std::string value) { _fileDir = value; }

    void SetCornerLowLeft(int x, int y) { _cornerLowLeft.x = x; _cornerLowLeft.y = y;  }

    const TPoint* GetCornerLowLeft();
    int GetWidth();
    int GetHeight();
    int GetCellSize();
    double* GetData() const;
    std::string GetName() const;

    H5::Group* CreateGroup(const H5::Group &parentGroup);

    bool ObtainPageNameZip (  );

    bool ReadGroup(const H5::Group& group, int name);
    double GetData(int x, int y);

    /* ====================  MUTATORS      ======================================= */

    /* ====================  OPERATORS     ======================================= */

  protected:
    /* ====================  METHODS       ======================================= */

    /* ====================  DATA MEMBERS  ======================================= */

  private:
    /* ====================  METHODS       ======================================= */
    char* Unzip( ); 

    inline bool String2Double(const std::string& str, double& result);

    void NumCols(int value) { _numCols = value; _width = _numCols * _cellSize; }
    void NumRows(int value) { _numRows = value; _height = _numRows * _cellSize; }
    void CellSize(int value) { _cellSize = value; _width = _numCols * _cellSize; _height = _numRows * _cellSize; }

    /* ====================  DATA MEMBERS  ======================================= */

    // related with the file
    std::string _fileDir;
    std::string _fileName;

    // related with the filedata
    int _numCols;
    int _numRows;
    TPoint _cornerLowLeft;
    int _cellSize;
    int _noDataValue;
    double *_values;

    // used to speed up
    int _width;
    int _height;
}; /* -----  end of class SpliceMap  ----- */
 
#endif
